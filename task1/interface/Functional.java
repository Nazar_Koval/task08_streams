package com.koval.task1;

@FunctionalInterface
public interface Functional {
    int func(int a, int b, int c);
}
