package com.koval;

import com.koval.task1.Functional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args){
        Functional functional1;
        Functional functional2;
        functional1 = (a,b,c) -> {int tmp = Math.max(a,b);
            return Math.max(tmp,c);
        };
        logger.trace(functional1.func(7,10,3) + "\n");

        functional2 = (a,b,c) -> (a+b+c)/3;
        logger.trace(functional2.func(7,11,3));
    }
}
