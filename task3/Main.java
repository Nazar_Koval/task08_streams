package com.koval;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);
    private static Random rd = new Random();

    private static Stream<Integer>generateFirst(int size){
        return Stream.generate(() -> rd.nextInt(100)).limit(size);
    }

    public static void main(String[] args) {
        int size = rd.nextInt(15);
        Stream<Integer> main = generateFirst(size);
        List<Integer> integerList = main.collect(Collectors.toList());
        logger.trace(integerList + "\n");
        //Statistics
        Stream<Integer> first = integerList.stream();
        IntSummaryStatistics statistics = first.mapToInt(x -> x).summaryStatistics();
        logger.trace(statistics);
        logger.trace("\n");

        //Sum using reduce
        Stream<Integer> second = integerList.stream();
        Optional<Integer> integerOptional = second.reduce((a, b) -> a + b);
        logger.trace(integerOptional);
        logger.trace("\n");

        //Sum using sum method
        Stream<Integer> third = integerList.stream();
        int optionalInt = third.mapToInt(x -> x).sum();
        logger.trace(optionalInt);
        logger.trace("\n");

        //Average and amount of numbers bigger than average
        Stream<Integer> fourth = integerList.stream();
        OptionalDouble average = fourth.mapToInt(x -> x).average();

        if (average.isPresent()) {
            long numberOf = integerList.stream().filter(el -> el > average.getAsDouble()).count();
            logger.trace(numberOf);
        }
    }
}
