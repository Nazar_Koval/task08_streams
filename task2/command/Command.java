package com.koval.task2;

@FunctionalInterface
public interface Command {
    void execute(final String str);
}
