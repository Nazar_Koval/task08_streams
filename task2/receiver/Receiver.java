package com.koval.task2;

public class Receiver {
    public void receive(final String str){
        new Logic().execute(str);
    }
}
