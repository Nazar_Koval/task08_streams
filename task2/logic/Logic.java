package com.koval.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Logic implements Command {
    private Logger logger =  LogManager.getLogger(Logic.class);

    @Override
    public void execute(final String str) {
        logger.trace("Command class string length : " + str.length() + "\n");
    }
}
