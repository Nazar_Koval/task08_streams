package com.koval;

import com.koval.task2.Command;
import com.koval.task2.Receiver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

public class Main  {

    private static Logger logger = LogManager.getLogger(Main.class);

    private static void execute(final String str){
        logger.trace("Main class string length : " + str.length() + "\n");
    }

    private static void exit(final String status){
        logger.trace("Exit with status " + status);
        System.exit(0);
    }
    public static void main(String[] args) {
        Map<String, Command> commandMap = new HashMap<>();

        commandMap.put("1", x -> logger.trace("Lambda string length : " + x.length() + "\n"));
        commandMap.put("2", new Command() {
            @Override
            public void execute(final String str) {
                logger.trace("Anonymous class string length : " + str.length() + "\n");
            }
        });
        commandMap.put("3", new Receiver()::receive);
        commandMap.put("4", Main::execute);
        commandMap.put("5", Main::exit);

        commandMap.get("1").execute("Random string");
        commandMap.get("2").execute("Other random string");
        commandMap.get("3").execute("One more string");
        commandMap.get("4").execute("The most random string");
        commandMap.get("5").execute("zero");
    }
}
