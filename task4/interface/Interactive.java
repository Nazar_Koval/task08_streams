package com.koval.task4;

@FunctionalInterface
public interface Interactive {
    void interact();
}
