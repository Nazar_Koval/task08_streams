package com.koval;

import com.koval.task4.Interactive;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main implements Interactive {

    private static Logger logger = LogManager.getLogger(Main.class);
    private static Scanner scanner = new Scanner(System.in);
    private static Stream<String> stringStream;

    @Override
    public void interact() {
        String text;
        Stream.Builder<String> streamBuilder = Stream.builder();

        do {
            text = scanner.nextLine();

            if (!text.isEmpty())
                streamBuilder.add(text);

        } while (!text.isEmpty());

        stringStream = streamBuilder.build();
    }

    private static boolean isStringUpperCase(String str){

        char[] charArray = str.toCharArray();

        for(int i=0; i < charArray.length; i++){

            if( !Character.isUpperCase( charArray[i] ))
                return false;
        }

        return true;
    }

    public static void main(String[] args) {

        new Main().interact();

        List<String> stringList = stringStream.collect(Collectors.toList());

        Stream<String> stringStream1 = stringList.stream();
        Set<String> stringSet = stringStream1.collect(Collectors.toSet());
        logger.trace("All unique words: " + stringSet + "\n");

        logger.trace("\n");

        Stream<String> stringStream2 = stringList.stream();
        List<String> sorted = stringStream2.distinct()
                .sorted()
                .collect(Collectors.toList());
        logger.trace("Sorted list with unique words: " + sorted + "\n");

        logger.trace("\n");

        logger.trace("Number of repeats for each word :\n");
        Stream<String> stringStream3;
        long amount;

        for (String str : stringList) {
            logger.trace(str + " - ");
            stringStream3 = stringList.stream();
            amount = stringStream3.filter(s -> s.equals(str)).count();
            logger.trace(amount + "\n");
        }
        logger.trace("\n");


        logger.trace("Number of repeats for each character:\n");
        Stream<String> stringStream4 = stringList.stream();
        List<String>characterList = stringStream4.map(x -> x.split(""))
                .flatMap(Arrays::stream)
                .collect(Collectors.toList());
        List<String>uniqueChar = characterList.stream()
                .distinct()
                .collect(Collectors.toList());
        long number;

        for (String ch:uniqueChar) {

            if(!isStringUpperCase(ch)) {
                logger.trace(ch + " - ");
                number = characterList.stream()
                        .filter(x -> x.equals(ch))
                        .count();
                logger.trace(number + "\n");
            }

        }
    }
}
